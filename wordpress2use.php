<?php
/**
 * @package Wordpress2use
 */
/*
Plugin Name: Wordpress2use
Plugin URI: http://bitbucket.org/hack2use/wordpress2use
Description: A collection of tools to boost the use of your WordPress site.
Version: 0.1.0
Author: Hack 2 Use
Author URI: http://hack2use.pl/
License: GPLv2
*/

require_once('entrypoint.php');

//require_once('shortcodes/gallery.php');
//require_once('shortcodes/gmaps.php');

function APIs_providers() {
    return array('facebook','foursquare','flickr','twitter');
}

foreach (APIs_providers() as $key) {
    require_once("APIs/$key/common.php");
}

require_once('backoffice/General.php');
require_once('backoffice/Cloud.php');
require_once('backoffice/Regenerate.php');
require_once('backoffice/RTL.php');

//require_once('widgets/CategoryBlock.php');
//require_once('widgets/GalleryBlock.php');

//require_once('widgets/PostsMap.php');
//require_once('widgets/PostMap.php');

require_once('contrib/SimpleSitemap.php');

