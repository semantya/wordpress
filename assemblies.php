<?php

function w2u_cdn_declare () {
    $PLUGIN_NAME = 'wordpress2use';
    
    wp_register_script('google-maps',            'http://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false');

    wp_register_script('modernizr',              jquery_plugin_url('flex', '/js/modernizr.js'));
	wp_enqueue_script( 'moment',                 jquery_plugin_url('fullcalendar','lib/moment.min.js'));

    wp_register_style('bootstrap',               w2u_assets('bootstrap/css/bootstrap.css'),array(),'','all');
    wp_register_style('bootstrap-theme',         w2u_assets('bootstrap/css/bootstrap-theme.css'),array('bootstrap'),'','all');
    wp_register_script('bootstrap',              w2u_assets('bootstrap/js/bootstrap.min.js'));

    wp_register_style('icons-fontawesome',       w2u_assets('icons/fontawesome/css/font-awesome.min.css'),array('bootstrap-theme'),'','all');
    wp_register_style('icons-metro',             w2u_assets('icons/metro/css/metro-bootstrap.css'),array('bootstrap-theme'),'','all');
    wp_register_style('icons-metro-responsive',  w2u_assets('icons/metro/css/metro-bootstrap-responsive.css'),array('icons-metro'),'','all');

    wp_register_script('jquery-latest',          w2u_assets('jquery/v1.11.1.min.js'));
    
    wp_register_style('jquery-jplayer-blue.monday',  jquery_plugin_url('jplayer', 'skins/blue.monday/jplayer.blue.monday.css'),array(),'','all');
	wp_register_script('jquery-jplayer',             jquery_plugin_url('jplayer', 'jquery.jplayer.min.js'));
	wp_register_script('jquery-jplaylist',             jquery_plugin_url('jplayer', 'jplayer.playlist.min.js'));

	#wp_register_script('jquery-ui',                  jquery_plugin_url('fullcalendar', 'lib/jquery-ui.custom.min.js'), array('jquery-latest'));
    wp_register_style('jquery-ui',                   'http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css', array(),'','all');
	wp_register_script('jquery-ui',                  'http://code.jquery.com/ui/1.10.3/jquery-ui.js', array('jquery-latest'));

    wp_register_style('jquery-chosen',               jquery_plugin_url('chosen', 'style.css'),array(),'','all');
    wp_register_script('jquery-chosen',              jquery_plugin_url('chosen', 'script.min.js'), array('jquery-latest'));

    wp_register_style('jquery-flex',                 jquery_plugin_url('flex', 'css/flexslider.css'),array(),'','all');
    wp_register_script('jquery-flex',                jquery_plugin_url('flex', 'js/jquery.flexslider-min.js'), array('jquery-latest'));
    
	wp_register_script('jquery-jcarousel',           jquery_plugin_url('jcarousel', 'script.min.js'));
	wp_register_style('jquery-jcarousel',            jquery_plugin_url('jcarousel', 'skin.css'));
    
	wp_register_script('jquery-datetime-picker',     jquery_plugin_url('datetime-picker', 'jquery.datetimepicker.js'));
	wp_register_style('jquery-datetime-picker',      jquery_plugin_url('datetime-picker', 'jquery.datetimepicker.css'));
    
	wp_register_style('fullcalendar',                jquery_plugin_url('fullcalendar', 'fullcalendar.min.css'), array(), null);
	wp_register_script('fullcalendar',               jquery_plugin_url('fullcalendar', 'fullcalendar.min.js'), array('jquery-latest','moment'));
	wp_register_script('fullcalendar-i18n',          jquery_plugin_url('fullcalendar', 'lang-all.js'), array('fullcalendar'));

	wp_register_script('amazingaudioplayer',         '/wp-content/themes/areview/player/audioplayerengine/amazingaudioplayer.js', array());

    wp_register_style('w2u-design',                  w2u_assets('design.css'),array(),'','all');
    wp_register_script('w2u-frontend',               w2u_assets('frontend.js'));
    wp_register_script('w2u-backend',                w2u_assets('backend.js'));
}

add_action('wp_enqueue_scripts', 'w2u_cdn_declare');

function w2u_cdn_header_common () {
    wp_enqueue_style("w2u-design");
}
function w2u_cdn_header_site () {
    w2u_cdn_header_common();

    wp_enqueue_script("jquery-latest");
    wp_enqueue_script("modernizr");

    wp_enqueue_style("bootstrap");
    wp_enqueue_style("bootstrap-theme");
    wp_enqueue_script("bootstrap");

    wp_enqueue_style("icons-fontawesome");
    wp_enqueue_style("icons-metro");
    wp_enqueue_style("icons-metro-responsive");

    wp_enqueue_style("jquery-jcarousel");
    wp_enqueue_script("jquery-jcarousel");

    wp_enqueue_style("jquery-jplayer-blue.monday");
    wp_enqueue_script("jquery-jplayer");
    wp_enqueue_script("jquery-jplaylist");
    wp_enqueue_script("amazingaudioplayer");
}
function w2u_cdn_header_admin () {
    //w2u_cdn_header_common();
    
    //wp_enqueue_script("w2u-backend");
}

add_action('wp_head',    'w2u_cdn_header_site');
add_action('admin_head', 'w2u_cdn_header_admin');

function w2u_cdn_footer_site () {
    wp_enqueue_script("w2u-frontend");
    
    if (is_home()) {
        wp_enqueue_script("theme-landing");
    }
}

add_action('wp_footer',    'w2u_cdn_footer_site');

