<?php

function w2u_seal_gmaps ($attrs) {
	extract( shortcode_atts( array(
	    'w'  => '100%',
	    'h'  => '250px',
	    'c' => '34.071231,-4.124879',
	    'z'  => '8',
	), $attrs ) );
    
    $resp = '<div id="map-'.$key.'" style="width: '.$w.'; height: '.$h.';"></div>';
    
    $resp .= '<script>';
    $resp .= 'jQuery(window).load(function() {';
    $resp .= '    Frontend.maps.show("map-'.$key.'", '.$z.', '.$c.');';
    $resp .= '});';
    $resp .= '</script>';
    
    return $resp;
}

add_shortcode('gmaps', 'w2u_seal_gmaps');

