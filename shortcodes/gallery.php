<?php

add_image_size('W2U_-slider', 575, 360, true);

function w2u_seal_gallery_flex ($output, $attrs) {
    global $post, $wp_locale;
    
    extract(shortcode_atts(array(
        'order'      => 'ASC',
        'orderby'    => 'menu_order ID',
        'id'         => $post->ID,
        'itemtag'    => 'dl',
        'icontag'    => 'dt',
        'captiontag' => 'dd',
        'columns'    => 3,
        'size'       => 'W2U_-slider',
        'include'    => '',
        'exclude'    => '',
        'type'       => 'slide',
        'loop'       => 'false'
    ), $attrs));
    
    $id = intval($id);
    if ( 'RAND' == $order )
        $orderby = 'none';

    if ( !empty($include) ) {
        $include = preg_replace( '/[^0-9,]+/', '', $include );
        $_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

        $lot = array();
        foreach ( $_attachments as $key => $val ) {
            $lot[$val->ID] = $_attachments[$key];
        }
    } elseif ( !empty($exclude) ) {
        $exclude = preg_replace( '/[^0-9,]+/', '', $exclude );
        $lot = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
    } else {
        $lot = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
    }
    
    if (in_array($type, array('slide', 'carousel')) or true) {
        $size = array(575, 360);
        
        $output = '<div class="flexslider"><ul class="slides">';
        
        foreach ($lot as $id => $att) {
            $link = isset($attr['link']) && 'file' == $attr['link'] ? wp_get_attachment_link($id, $size, false, false) : wp_get_attachment_link($id, $size, true, false);
            
            $output .= '<li>'.$link.'</li>';
        }
        
        $output .= '</ul></div>';
        
        $output .= '<script>';
        $output .= 'jQuery(window).load(function() {';
        $output .= '    jQuery(".flexslider").flexslider({';
        $output .= '        animation: "'.$type.'",';
        $output .= '        animationLoop: '.$loop.',';
        $output .= '    });';
        $output .= '});';
        $output .= '</script>';
    }
    
    return $output;
}

add_filter( 'post_gallery', 'w2u_seal_gallery_flex', 10, 2 );

