<?php

class TwitterConfigForm extends W2U_Form {
	private $formats = array();

    protected function prepare () {
        $this->field('api_key', 'string')->clean(function ($field, $value) {
            return esc_attr($value);
        });
        $this->field('api_secret', 'text')->clean(function ($field, $value) {
            return esc_attr($value);
        });
        $this->field('username', 'string')->clean(function ($field, $value) {
            return esc_attr($value);
        });
    }
}

