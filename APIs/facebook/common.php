<?php

class FacebookConfigForm extends W2U_Form {
	private $formats = array();

    protected function prepare () {
        $this->field('api_key', 'string')->clean(function ($field, $value) {
            return esc_attr($value);
        });
        $this->field('api_secret', 'text')->clean(function ($field, $value) {
            return esc_attr($value);
        });
        $this->field('page_id', 'string')->clean(function ($field, $value) {
            return esc_attr($value);
        });
        $this->field('access_token', 'string')->clean(function ($field, $value) {
            return esc_attr($value);
        });
    }
}

