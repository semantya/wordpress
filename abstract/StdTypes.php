<?php

class W2U_Field_text extends W2U_Field {
    public function render_std ($value) {
?>
    <br />
	<textarea id="<?php echo $this->wp_field_id() ?>" name="<?php echo $this->wp_field_name() ?>"><?php echo $value ?></textarea>
<?php
    }
}

/*************************************************************************************************************************************/

class W2U_Field_string extends W2U_Field {
    public $std_type = 'text';
    
    public function render_std ($value) {
?>
	<input id="<?php echo $this->wp_field_id() ?>" class="widefat" name="<?php echo $this->wp_field_name() ?>" type="<?php echo $this->std_type; ?>" value="<?php echo $value; ?>">
<?php
    }
}

/*************************************************************************************************************************************/

class W2U_Field_integer extends W2U_Field {
    public $std_type = 'number';
    
    public function render_std () {
?>
	<input id="<?php echo $this->wp_field_id() ?>" class="widefat" name="<?php echo $this->wp_field_name() ?>" type="<?php echo $this->std_type; ?>" value="<?php echo $value; ?>">
<?php
    }
}

/*************************************************************************************************************************************/

class W2U_Field_boolean extends W2U_Field {
    public $std_type = 'number';
    
    public function render_std () {
?>
	<input id="<?php echo $this->wp_field_id() ?>" class="widefat" name="<?php echo $this->wp_field_name() ?>" type="<?php echo $this->std_type; ?>" value="<?php echo $this->default; ?>">
<?php
    }
}

