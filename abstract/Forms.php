<?php

abstract class W2U_Field {
    protected $form;
    protected $name;
    
    public $label;
    public $default;
    
    protected $choices = null;
    protected $validators = array();
    protected $cleaners = array();
    
	public function __construct ($form, $name, $default) {
        $this->form    = $form;
        $this->name    = $name;
        $this->default = $default;
        
        $this->label   = ucfirst($name);
    }
    
    public function name   () { return $this->name; }
    public function form   () { return $this->form; }
    public function widget () { return $this->form()->widget(); }
    
    public function wp_field_id () {
        return esc_attr( $this->widget()->get_field_id($this->name) );;
    }
    public function wp_field_name () {
        return esc_attr( $this->widget()->get_field_name($this->name) );;
    }
    
    public function validators () { return $this->validators; }
    public function cleaners   () { return $this->cleaners; }
    
    public function render ($current) {
        if ($current==null) {
            $current = $this->default;
        }
        
        ?><label for="<?php echo $this->wp_field_id() ?>"><?php $this->label_i18n(); ?></label><?php
        
        if (is_array($this->choices)) {
?>
	<select id="<?php echo $this->wp_field_id() ?>" class="widefat" name="<?php echo $this->wp_field_name() ?>">
<?php foreach ( $this->choices as $key => $value ) : ?>
		<option value="<?php echo $key ?>"<?php echo ($key==$current)?' selected':'' ?>><?php echo $value ?></option>
<?php endforeach; ?>
	</select>
<?php
        } else {
            $this->render_std($current);
        }
    }
    
    /*******************************************************************************************/
    
    public function label_i18n () { return _e($this->label, 'wordpress2use'); }
    public function rename ($new_title) {
        $this->label = $new_title;
        
        return $this;
    }
    
    public function choices ($bucket) {
        if (($this->choices==null) and (is_array($bucket))) {
            $this->choices = $bucket;
        }
        
        return $this;
    }
    
    /*******************************************************************************************/
    
    public function validate ($callback) {
        
        return $this;
    }
    public function clean ($callback) {
        
        return $this;
    }
}

abstract class W2U_Form {
    protected $widget;
    protected $name;
    protected $fields = array();
    protected $lock = false;
    
	public function __construct ($widget, $name) {
        $this->widget  = $widget;
        $this->name    = $name;
        $this->fields  = array();
        
        $this->prepare();
        
        $this->lock = true;
    }
    
    public function widget () { return $this->widget; }
    
    public function field ($name, $type, $default=null) {
        if ($this->lock==false) {
            $cls = "W2U_Field_{$type}";
            
            if (class_exists($cls)) {
                $field = new $cls($this, $name, $default);
                
                $this->fields[$name] = $field;
                
                return $field;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
    
    public function fields () { return $this->fields; }
    
    public function validate ($payload) {
        $resp = true;
        
        foreach ($this->fields as $field) {
            foreach ($field->validators() as $callback) {
                $value = $field->default;
                
                if (array_key_exists($field->name, $payload)) {
                    $value = $payload[$field->name];
                }
                
                $resp = $resp and $callback($field, $value);
            }
        }
        
        return $resp;
    }
    
    public function clean ($payload) {
        $resp = array();
        
        foreach ($this->fields as $field) {
            $value = $field->default;
            
            if (array_key_exists($field->name(), $payload)) {
                $value = $payload[$field->name()];
            }
            
            foreach ($field->cleaners() as $callback) {
                $value = $callback($field, $value);
            }
            
            $resp[$field->name()] = $value;
        }
        
        return $resp;
    }
    
    public function render ($args) {
        if (!array_key_exists('data', $args)) {
            $args['data'] = array();
        }
        
        if (!is_array($args['data'])) {
            $args['data'] = array();
        }
        
?>
    <form method="<?php echo $args['verb'] ?>" action="<?php echo $args['link'] ?>">
<?php
        
        echo $args['before_form'];
        
        foreach ($this->fields() as $field) {
            echo $args['before_field'];
            
            if (!array_key_exists($field->name(), $args['data'])) {
                $args['data'][$field->name()] = $field->default;
            }
            
            echo $field->render($args['data'][$field->name()]);
            
            echo $args['after_field'];
        }
        
        echo $args['after_form'];
        
?>
    </form>
<?php
    }
}

