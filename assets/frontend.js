var Frontend = {
    bootstrap: function () {
        if ($('.chzn-area select').chosen) {
            $('.chzn-area select').chosen({
                allow_single_deselect: true,
            });
        }
        
        //$('form select').chosen();
        
        $('ul.qtrans_language_chooser').parent().children('h3').remove();
    },
    maps: {
        objs: {},
        bare: function (target) {
            var map = new google.maps.Map(document.getElementById(target),{
                mapTypeId: google.maps.MapTypeId.HYBRID,
            });
            
            Frontend.maps.objs[target] = map;
            
            return map;
        },
        mark: function (map, meta) {
            meta.pos = new google.maps.LatLng(meta.lat, meta.lng);
            
            wnd = new google.maps.InfoWindow({
                content: meta.content,
            });
            
            mrk = new google.maps.Marker({
                title: meta.title,
                position: meta.pos,
                map: map,
                infoWindow: wnd,
                visible: true,
            });
            
            google.maps.event.addListener(mrk, 'click', function() {
                wnd.open(map, mrk);
            });
            
            return mrk;
        },
        highlight: function (map, zoom, lng, lat) {
            pos = new google.maps.LatLng(lng, lat);
            
            map.setZoom(zoom);
            map.setCenter(pos);
            
            return map;
        },
        show: function (target, zoom, lng, lat) {
            var map = Frontend.maps.bare(target);
            
            Frontend.maps.highlight(map, zoom, lng, lat);
            
            Frontend.maps.mark(map, {
                title: 'Tazekka Parc Hotel',
                content: 'Tazekka Parc Hotel',
                lng: lng,
                lat: lat,
            });
            
            Frontend.maps.objs[target] = map;
            
            /*
            map = Frontend.maps.bare(target);
            
            var lst = [
                //{ title:"Berkane", lat: 34.919558,  lng: -2.325121, type: 'estivage' },
            ];
            
            for (var i=0 ; i<lst.length ; i++) {
                mrk = Frontend.maps.mark(map, lst[i]);
                
                mrk.extra_data_index = i;
                
                lst[i].marker = mrk;
            }
            
            Frontend.maps.highlight(map, 13, 34.071231, -4.124879);
            //*/
            
            return map;
        },
    },
};

jQuery(function () {
    Frontend.bootstrap();
});

