<?php

function call_http ($verb, $url) {
    $chandle = curl_init();
    curl_setopt($chandle, CURLOPT_URL, $url);
    curl_setopt($chandle, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($chandle);
    curl_close($chandle);
    
    return $result;
}
function call_json ($verb, $url) {
    $resp = call_http($verb, $url);
    
    $data = json_decode($resp);
    
    return $data;
}
function call_xml ($verb, $url) {
    $resp = call_http($verb, $url);
    
    $data = xml_decode($resp);
    
    return $data;
}

