<?php

define('CrLf', "\n");

require_once('shortcuts.php');
require_once('helpers.php');
require_once('assemblies.php');

require_once('abstract/Forms.php');
require_once('abstract/StdTypes.php');

require_once('common/Plugin.php');
require_once('common/Type.php');
require_once('common/Widget.php');
require_once('common/AdminPage.php');

