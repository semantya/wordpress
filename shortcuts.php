<?php

function template_url($path) {
	return get_template_directory_uri().$path;
}

function template_dir($path) {
	return get_template_directory_uri().$path;
}

function plugin_url($name,$path) {
	return plugins_url(implode('/',array($name,$path)));
}

function plugin_dir($name,$path) {
	return implode('/',array(dirname(plugin_dir_path(__FILE__)),$name,$path));
}

###########################################################################

function w2u_assets($path) {
	return plugins_url(implode('/',array('wordpress2use','assets',$path)));
}

function jquery_plugin_url($cmp,$path) {
	return w2u_assets("jquery/-/{$cmp}/{$path}");
}

###########################################################################

function startswith($haystack, $needle) {
    return $needle === "" || strpos($haystack, $needle) === 0;
}

function endswith($haystack, $needle) {
    return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
}

