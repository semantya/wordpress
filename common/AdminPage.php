<?php

abstract class W2U_AdminPage {
	private $menu_id;

    function __construct() {
		load_plugin_textdomain( 'regenerate-thumbnails', false, '/regenerate-thumbnails/localization' );

		add_action( 'admin_menu',                              array( &$this, 'add_admin_menu' ) );
		add_action( 'admin_enqueue_scripts',                   array( &$this, 'admin_enqueues' ) );
		add_action( 'wp_ajax_regeneratethumbnail',             array( &$this, 'ajax_process_image' ) );
		add_filter( 'media_row_actions',                       array( &$this, 'add_media_row_action' ), 10, 2 );
		//add_filter( 'bulk_actions-upload',                     array( &$this, 'add_bulk_actions' ), 99 ); // A last minute change to 3.1 makes this no longer work
		add_action( 'admin_head-upload.php',                   array( &$this, 'add_bulk_actions_via_javascript' ) );
		add_action( 'admin_action_bulk_regenerate_thumbnails', array( &$this, 'bulk_action_handler' ) ); // Top drowndown
		add_action( 'admin_action_-1',                         array( &$this, 'bulk_action_handler' ) ); // Bottom dropdown (assumes top dropdown = default value)

        /*
        add_action('wp_loaded',    array($this, 'cdn_declare'));
        add_action('wp_head',      array($this, 'cdn_site'));
        add_action('admin_head',   array($this, 'cdn_admin'));
        */

		$this->capability = apply_filters( 'cloud2use_capability', 'manage_options' );
    }
    
    private function rpath($chemin) {
        return plugin_dir($this->name, $chemin);
    }
    
    private function url($chemin) {
        return plugin_url($this->name, $chemin);
    }

    /***************************************************************************************************************/
    
    public function get_field_id ($value) {
        return $value;
    }
    
    public function get_field_name ($value) {
        return $value;
    }
}

