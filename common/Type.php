<?php

abstract class W2U_Type {
    function __construct() {
        add_action('init',           array($this, 'register_type'));
        
        add_action('add_meta_boxes', array($this, 'register_metabox'));
        
        foreach ($this->metaboxes as $mbox) {
            add_action('save_post', array($this, "metabox_{$mbox['name']}_process"));
        }
    }
    
    /***************************************************************************************************************/

    function register_type () {
        register_post_type($this->name, $this->options);
    }
    
    /***************************************************************************************************************/
    
    function register_metabox () {
        foreach ($this->metaboxes as $mbox) {
	        add_meta_box("metabox_{$mbox['name']}", $mbox['title'], array($this, "metabox_{$mbox['name']}_callback"), $mbox['types']);
        }
    }
}

