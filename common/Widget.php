<?php

abstract class W2U_Widget extends WP_widget {
    private static $loaded = null;
    private static $factory = array();
    
    static function register ($cls) {
        if (!in_array($cls, W2U_Widget::$factory)) {
            W2U_Widget::$factory[] = $cls;
            
            if (W2U_Widget::$loaded!=null) {
                register_widget($cls);
                
                W2U_Widget::$loaded[] = $cls;
            }
        }
    }
    
    static function bootstrap () {
        if (W2U_Widget::$loaded==null) {
            W2U_Widget::$loaded = array(); 
            
            foreach (W2U_Widget::$factory as $cls) {
                register_widget($cls);
                
                W2U_Widget::$loaded[] = $cls;
            }
        }
    }
    
    function __construct() {
        $opts = $this->w2u_options;
        
        if (!(array_key_exists('title', $opts) and isset($opts['title']))) {
            $opts['title'] = $ops['description'];
        }
        
        $this->WP_widget($opts['classname'], $opts['title'], $opts);
        
        $this->w2u_prepare();
    }
    
    function widget ($arguments, $data) {
        foreach ($this->w2u_defaults as $k => $v) {
            if (is_string($data[$k]) and !strlen(trim($data[$k]))) {
                $data[$k] = $v;
            }
        }
        
        $st = ($data['on_prod'])?true:false;
        
        /*
        $st = ($st and w2u_role($this->w2u_role)) or !$st;
        //*/
        
        if ($st or is_super_admin()) {
	        $title  = apply_filters( 'widget_title', empty( $data['title'] ) ? "" : $data['title'], $data, $this->id_base );

            /**************************************************************************************************************/
            
		    echo $arguments['before_widget'];
            
            if (0 < strlen($title)) {
                echo $arguments['before_title'].$title.$arguments['after_title'];
            }
            
            $this->w2u_render($arguments, $data);
            
		    echo $arguments['after_widget'];
        }
    }
    
    function form ($data) {
        foreach ($this->w2u_defaults as $k => $v) {
            if (!array_key_exists($k, $data)) {
                $data[$k] = $v;
            }
        }
        
        $this->w2u_form($data);
?>
    <p>
        <label for="<?php echo $this->get_field_id('on_prod'); ?>">Is debug ?</label><br />
        <input name="<?php echo $this->get_field_name('on_prod'); ?>" id="<?php echo $this->get_field_id('on_prod'); ?>" type="checkbox" <?php echo ($data['on_prod'])?'checked ':''; ?>/>
    </p>
<?php
    }
    
    function update ($content_new, $content_old) {
        $resp = $this->w2u_update($content_new, $content_old);
        
        $resp['beta_test']  = ($content_new['beta_test'])?true:false;
        
        return $resp;
    }
    
}

class W2U_FormWidget extends W2U_Widget {
    protected $form = null;
    
	function w2u_form ($instance) {
        foreach ($this->form->fields() as $field) {
            $value = null;
            
            if (array_key_exists($field->name(), $instance)) {
                $value = $instance[$field->name()];
            }
            
            echo '<p>';
            $field->render($value);
            echo '</p>';
        }
	}
    
	function w2u_update($payload, $bucket) {
        if ($this->form->validate($payload)) {
            $data = $this->form->clean($payload);
            
            return array_merge($bucket, $data);
        } else {
            return $bucket;
        }
	}
}


add_action("widgets_init", "W2U_Widget::bootstrap");

