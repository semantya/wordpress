<?php

abstract class W2U_Plugin {
    function __construct() {
        foreach (array('types', 'routes') as $aspect) {
            foreach ($this->lsdir($aspect, '.php') as $pth) {
                require_once($this->rpath("{$aspect}/{$pth}"));
            }
        }
        
        add_action('widgets_init',       array($this, 'widgets_init'));
        add_action('wp_enqueue_scripts', array($this, 'cdn_declare'));
        add_action('wp_head',            array($this, 'cdn_site'));
        add_action('admin_head',         array($this, 'cdn_admin'));
    }
    
    private function rpath($chemin) {
        return plugin_dir($this->name, $chemin);
    }
    
    private function url($chemin) {
        return plugin_url($this->name, $chemin);
    }
    
    private function lsdir($rpath, $suffix="") {
        $resp = array();
        
        foreach ( scandir($this->rpath($rpath)) as $pth) {
            if ($suffix=="" or endswith($pth, $suffix)) {
                $resp[] = $pth;
            }
        }
        
        return $resp;
    }
    
    public function widgets_init() {
        foreach ($this->lsdir('widgets', '.php') as $pth) {
            require_once($this->rpath("widgets/{$pth}"));
            
            $key = str_replace('.php', '', $pth);
            
            register_widget("RoumayssaGoodies_{$key}_Widget");
        }
    }

    /***************************************************************************************************************/

}

