<?php
class W2U_Mapping extends W2U_Widget
{
    protected $w2u_options = array(
        "classname"   => "W2U_Mapping",
        "title"       => "GMap 2 Use",
        "description" => "A map which displays marker of a post type's custom field.",
    );
    
    protected $w2u_defaults = array(
        'center'    => '34.224294,-4.006433',
        'metatype'  => 'post',
        'zoom'      => 5,
    );
    
    //protected $w2u_role = 'ads';
    
    function w2u_update ($content_new, $content_old) {
        $content_new['zoom'] = esc_attr($content_new['zoom']);
        
        $content_new['metatype'] = esc_attr($content_new['metatype']);
        
        if (is_array($content_new['center'])) {
            $content_new['center'] = implode(',', $content_new['center']);
        }
        
        $content_new['center'] = esc_attr($content_new['center']);
        
        return $content_new;
    }
    
    function w2u_form ($data) {
        $pos = explode(',', $data['center']);
?>
    <p>
        <label for="<?php echo $this->get_field_id('zoom'); ?>">Map zoom :</label><br />
        <input style="width: 100%;" type="geo" name="<?php echo $this->get_field_name('zoom'); ?>" id="<?php echo $this->get_field_id('center'); ?>" value="<?php echo $data['zoom']; ?>" />
        <hr />
        <label for="<?php echo $this->get_field_id('center'); ?>">Map center :</label><br />
        <input style="width: 100%;" type="geo" name="<?php echo $this->get_field_name('center'); ?>" id="<?php echo $this->get_field_id('center'); ?>" value="<?php echo $data['center']; ?>" />
        <hr />
        <label for="<?php echo $this->get_field_id('metatype'); ?>">Post Type :</label><br />
        <select style="width: 100%;" class="chzn-sel" name="<?php echo $this->get_field_name('metatype'); ?>" id="<?php echo $this->get_field_id('metatype'); ?>">
<?php
        $qs = get_post_types('', 'objects');
        
        foreach ($qs as $pt) {
?>
            <option value="<?php echo $pt->name; ?>" <?php if ($pt->name==$data['metatype']) { echo 'selected'; } ?>><?php echo $pt->name ?></option>
<?php } ?>
        </select>
    </p>
<?php
    }
    function w2u_render ($arguments, $data) {
        global $wpdb;
        $table_prefix = $wpdb->prefix;
        
        extract($arguments);
        
        echo $before_widget;
        
        $resp  = "    var map = Frontend.maps.bare('homemap');\n";
        
        $resp .= "    Frontend.maps.highlight(map, parseInt({$data['zoom']}), {$data['center']});\n";
        
        $qs = get_posts(array(
            'order' => 'ASC',
            'orderby' => 'title',
            'post_type' => 'ville', //$data['metatype'],
            'posts_per_page' => -1,
        ));
        
        foreach ($qs as $p) {
            $pos = array(
                get_post_meta($p->ID, 'geo_longitude', true),
                get_post_meta($p->ID, 'geo_latitude', true),
            );
            
            if (2<=sizeof($pos)) {
                if ($pos[0] and $pos[1]) {
                    $resp .= "    ".CrLf;
                    $resp .= "    Frontend.maps.mark(map, {\n";
                    $resp .= "        title:   \"{$p->post_title}\",\n";
                    $resp .= "        content: \"{$p->post_excerpt}\",\n";
                    $resp .= "        lng:     {$pos[0]},\n";
                    $resp .= "        lat:     {$pos[1]},\n";
                    $resp .= "    });\n";
                }
            }
        }
?>
        <div id="homemap" style="width: 100%; height: 450px;"></div>
        <script type="text/javascript">
jQuery(window).load(function() {
<?php echo $resp; ?>
});
        </script>
<?php
        echo $after_widget;
    }
}

W2U_Widget::register("W2U_Mapping");

