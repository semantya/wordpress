<?php
class W2U_GalleryBlock extends W2U_Widget
{
    protected $w2u_options = array(
        "classname"   => "W2U_GalleryBlock",
        "title"       => "Landing-page Gallery Widget",
        "description" => "Affiche une gallerie en slider sur la page d'acceuil.",
    );
    
    protected $w2u_defaults = array(
        'gallery_ids' => '',
    );
    
    protected $w2u_role = 'ads';
    
    function w2u_update ($content_new, $content_old) {
        if (is_array($content_new['gallery_ids'])) {
            $content_new['gallery_ids'] = implode(',', $content_new['gallery_ids']);
        }
        
        $content_new['gallery_ids'] = esc_attr($content_new['gallery_ids']);
        
        return $content_new;
    }
    
    function w2u_form ($data) {
        $sel = explode(',', $data['gallery_ids']);
?>
    <p>
        <label for="<?php echo $this->get_field_id('gallery_ids'); ?>">Gallery :</label><br />
        <select class="chzn-sel" multiple name="<?php echo $this->get_field_name('gallery_ids'); ?>" id="<?php echo $this->get_field_id('gallery_ids'); ?>">
<?php
        $qs = new WP_Query(array(
            'post_type' => 'gallery',
            'orderby'   => 'date',
            'order'     => 'DESC',
        ));
        
        foreach ($qs as $p) {
?>
            <option value="<?php echo $p->post_ID; ?>" <?php if (in_array($p->post_ID, $sel)) { echo 'selected'; } ?>><?php echo $p->post_title ?></option>
<?php } ?>
        </select>
    </p>
<?php
    }
    //*/
    
    function w2u_render ($arguments, $data) {
        global $wpdb;
        $table_prefix = $wpdb->prefix;
        
        extract($arguments);
        
        echo $before_widget;
?>
<div class="container">
    <div class="sixteen columns">
        <div class="flexslider">
            <ul class="slides">
<?php
        foreach ($qs as $p) {
?>
                <li>
                    <?php the_post_thumbnail(); ?>
                    <p class="flex-caption"></p>
                </li>
<?php
        }
?>
            </ul>
        </div>
    </div>
</div>
<?php
        echo $after_widget;
    }
}

W2U_Widget::register("W2U_GalleryBlock");

