<?php
class W2U_Map extends W2U_Widget
{
    protected $w2u_options = array(
        "classname"   => "W2U_Map",
        "title"       => "Enochan Location",
        "description" => "A map which displays marker of a post.",
    );
    
    protected $w2u_defaults = array(
        'metatypes' => 'post,vehicule',
        'access' => '',
        'zoom'   => 5,
    );
    
    //protected $w2u_role = 'ads';
    
    function w2u_update ($content_new, $content_old) {
        $content_new['zoom'] = esc_attr($content_new['zoom']);
        
        $content_new['access'] = esc_attr($content_new['access']);
        
        $content_new['metatypes'] = implode(',', $content_new['metatypes']);
        
        return $content_new;
    }
    
    function w2u_form ($data) {
?>
    <p>
        <label for="<?php echo $this->get_field_id('zoom'); ?>">Map zoom :</label><br />
        <input style="width: 100%;" type="geo" name="<?php echo $this->get_field_name('zoom'); ?>" id="<?php echo $this->get_field_id('center'); ?>" value="<?php echo $data['zoom']; ?>" />
        <hr />
        <label for="<?php echo $this->get_field_id('access'); ?>">Access Path :</label><br />
        <input style="width: 100%;" name="<?php echo $this->get_field_name('access'); ?>" id="<?php echo $this->get_field_id('center'); ?>" value="<?php echo $data['access']; ?>" />
        <hr />
        <label for="<?php echo $this->get_field_id('metatypes'); ?>">Post Type :</label><br />
        <select multiple style="width: 100%;" class="chzn-sel" name="<?php echo $this->get_field_name('metatypes'); ?>" id="<?php echo $this->get_field_id('metatypes'); ?>">
<?php
        $qs = get_post_types('', 'objects');
        
        $data['mtypes'] = explode(',', $data['metatypes']);
        
        foreach ($qs as $pt) {
?>
            <option value="<?php echo $pt->name; ?>" <?php if (in_array($pt->name, $data['mtypes'])) { echo 'selected'; } ?>><?php echo $pt->name ?></option>
<?php } ?>
        </select>
    </p>
<?php
    }
    function w2u_render ($arguments, $data) {
        global $wpdb;
        $table_prefix = $wpdb->prefix;
        
        extract($arguments);
        
        $mtps = explode(',', $data['metatypes']);
        
	if (true or in_array(get_post_type(), $mtps)) {
	        echo $before_widget;
        	
        	$orig = get_post(); $curr = $orig; $i = 0;
        	
                if (strlen($data['access'])) {
 	            $path = explode(',', $data['access']);
        	    
        	    while ($curr!=null and ($i<sizeof($path))) {
        	        //echo "<li>{$curr->ID} : {$curr->post_title}</li>";
        	        
        	        $uid = get_post_meta($curr->ID, $path[$i], true);
        	        
        	        $curr = get_post($uid);
        	        
        	        $i += 1;
        	    }
        	}
        	
 		if (isset($curr)) {
	            //echo $before_widget;
        	    
	            $resp  = "    var map = Frontend.maps.bare('homemap');\n";
        	    
        	    $pos = array(
                	get_post_meta($curr->ID, 'geo_latitude', true),
                	get_post_meta($curr->ID, 'geo_longitude', true),
	            );
            	    
                    if (2<=sizeof($pos)) {
	                $resp .= "    Frontend.maps.highlight(map, parseInt({$data['zoom']}), {$pos[0]}, {$pos[1]});\n";
                   }
		if ($orig->ID!=$curr->ID) {
?>
        <h1><b><?php echo ucfirst(get_post_type($curr->ID)); ?></b> : <a href="<?php echo get_permalink($curr->ID); ?>"><?php echo $curr->post_title; ?></a></h1>
<?php
		}
?>
        <div id="homemap" style="width: 100%; height: 250px;"></div>
        <script type="text/javascript">
jQuery(window).load(function() {
<?php echo $resp; ?>
});
        </script>
<?php
                //echo $after_widget;
            }
            
            echo $after_widget;
        }
    }
}

W2U_Widget::register("W2U_Map");
