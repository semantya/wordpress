<?php
class W2U_CategoryBlock extends W2U_Widget
{
    protected $w2u_options = array(
        "classname" => "W2U_CategoryBlock",
        "title" => "Category Block",
    );
    
    protected $w2u_defaults = array(
        'main' => array(
            'row_count'  => 4,
            'meta_field' => 'featured',
            'order_by'   => 'date',
            'order_in'   => 'DESC',
        ),
        'media' => array(
            'row_count'  => 25,
        ),
        'news' => array(
            'row_count'  => 15,
            'meta_field' => 'slideshow',
            'order_by'   => 'date',
            'order_in'   => 'DESC',
        ),
        'slider' => array(
            'row_count'  => 10,
        ),
        'sub' => array(
            'row_count'  => 5,
        ),
    );
    
    protected $w2u_role = 'publisher';
    
    function w2u_render ($arguments, $data) {
        $prefix = str_replace('-', '_', $data['categ_id']);
        $selector = "W2U_CategoryBlock_{$prefix}";
        
        $catinfo = get_category($data['categ_id']);
        
        if (strlen($data['title'])==0) {
            $data['title'] = get_cat_name($catinfo->cat_ID);
        }
        
        extract($arguments);
        
        $data['title'] = $data['title'] or $catinfo->nicename;
        
        $opts = array(
            'title' => '<a href="'.get_category_link($catinfo->cat_ID).'">'.$data['title'].'</a>',
            'args'  => $arguments,
            'cfg'   => $data,
        );
        
        $lst = array_keys(W2U_CategoryBlock::$kinds);
        
        $lkp = array(
            'showposts' => $data['row_count'],
            'orderby'   => 'date',
            'order'     => 'DESC',
        );
        
        if (($data['categ_type'])?true:false) {
            $lkp['cat'] = $catinfo->term_id;
        }
        
        if (array_key_exists($data['meta_field'], W2U_CategoryBlock::$metafields)) {
            $lkp['meta_key']   = $data['meta_field'];
            $lkp['meta_value'] = '1';
        }
        
        if (in_array($data['order_by'], W2U_CategoryBlock::$axis)) {
            $lkp['orderby']    = $data['order_by'];
        }
        
        if (in_array($data['order_in'], array('ASC', 'DESC'))) {
            $lkp['order']      = $data['order_in'];
        }
        
        if (in_array($data['view_type'], $lst)) {
            $qs = new WP_Query($lkp);
            
            call_user_func("W2U_CategoryBlock_{$data['view_type']}", $opts, $catinfo, $qs, $lkp);
        }
    }
    
    function w2u_update ($content_new, $content_old) {
        $content_new['title']      = esc_attr($content_new['title']);
        $content_new['categ_id']   = intval($content_new['categ_id']);
        $content_new['row_count']  = intval($content_new['row_count']) or null;
        $content_new['meta_field'] = $content_new['meta_field'] or null;
        $content_new['view_type']  = $content_new['view_type'];
        $content_new['order_by']   = $content_new['order_by'] or null;
        $content_new['order_in']   = $content_new['order_in'] or null;
        $content_new['categ_type'] = ($content_new['categ_type'])?true:false;
        
        /*
        foreach (array('row_count', 'meta_field', 'order_by', 'order_in') as $key) {
            if (in_array($content_new[$k], array('', null)) and array_key_exists($k, W2U_CategoryBlock::$defaults[$content_new['view_type']])) {
                $content_new[$k] = W2U_CategoryBlock::$defaults[$content_new['view_type']][$k];
            }
        }
        //*/
        
        return $content_new;
    }
    
    function w2u_form ($data) {
?>
    <p>
        <label for="<?php echo $this->get_field_id('title'); ?>">Titre :</label><br />
        <input type="text" name="<?php echo $this->get_field_name('title'); ?>" id="<?php echo $this->get_field_id('title'); ?>" value="<?php echo $data['title']; ?>"/>
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('categ_type'); ?>">Categorie :</label><br />
        <input type="checkbox" name="<?php echo $this->get_field_name('categ_type'); ?>" id="<?php echo $this->get_field_id('categ_type'); ?>" <?php echo ($data['categ_type'])?'checked ':''; ?>/>
        <?php wp_dropdown_categories( 'selected='.$data['categ_id'].'&name='.$this->get_field_name('categ_id').'&hierarchical=1'); ?>
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('meta_field'); ?>">Meta-type :</label><br />
        <select name="<?php echo $this->get_field_name('meta_field'); ?>" id="<?php echo $this->get_field_id('meta_field'); ?>">
            <option value="<?php echo $key ?>"<?php echo ($key=='none')?' selected':''; ?>>- Aucun -</option>
<?php foreach (W2U_CategoryBlock::$metafields as $key => $label) { ?>
            <option value="<?php echo $key ?>"<?php echo ($key==$data['meta_field'])?' selected':''; ?>><?php echo $label ?></option>
<?php } ?>
        </select>
    </p>
    <hr/>
    <p>
        <label for="<?php echo $this->get_field_id('order_by'); ?>">Order by :</label><br />
        <select name="<?php echo $this->get_field_name('order_by'); ?>" id="<?php echo $this->get_field_id('order_by'); ?>">
<?php foreach (W2U_CategoryBlock::$axis as $key => $label) { ?>
            <option value="<?php echo $key ?>"<?php echo ($key==$data['order_by'])?' selected':''; ?>><?php echo $label ?></option>
<?php } ?>
        </select>
<?php foreach (array('ASC', 'DESC') as $key) { ?>
        <br/><input type="radio" name="<?php echo $this->get_field_name('order_in'); ?>" id="<?php echo $this->get_field_id('order_in'); ?>" value="<?php echo $key ?>" <?php echo ($data['order_in']==$key)?'checked':''; ?> /> <?php echo $key ?>
<?php } ?>
    </p>
    <hr/>
    <p>
        <label for="<?php echo $this->get_field_id('row_count'); ?>">Nbr de lignes :</label><br />
        <input type="spinner" name="<?php echo $this->get_field_name('row_count'); ?>" id="<?php echo $this->get_field_id('row_count'); ?>" value="<?php echo $data['row_count']; ?>"/>
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('view_type'); ?>">Type de vue :</label><br />
        <select name="<?php echo $this->get_field_name('view_type'); ?>" id="<?php echo $this->get_field_id('view_type'); ?>">
<?php foreach (W2U_CategoryBlock::$kinds as $key => $label) { ?>
            <option value="<?php echo $key ?>"<?php echo ($key==$data['view_type'])?' selected':''; ?>><?php echo $label ?></option>
<?php } ?>
        </select>
    </p>
<?php
    }
    //*/
    
    static $kinds = array(
        'landing'    => "Welcome page",
        'category'   => "Sub-category",
        'page'       => "Multimedia Slider",
        'single'     => "Video thumbnails",
    );
    
    static $metafields = array(
        'featured'  => "Featured content",
        'vindex'    => "Homepage video",
        'slideshow' => "Slide-show",
    );
    
    static $axis = array(
        'rand'          => "Random ordering",
        'date'          => "By published date",
        'modified'      => "By modified date",
        'title'         => "By title",
        'comment_count' => "By comments",
    );
}

W2U_Widget::register("W2U_CategoryBlock");

